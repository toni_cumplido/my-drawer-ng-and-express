import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular/router';
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';
import * as app from "tns-core-modules/application";

import { NoticiasService } from "../domain/noticias.service";
import { Noticia, LeerNoticiaAction } from "../domain/noticias-state.model";
import { Store, State } from '@ngrx/store';
import { AppState } from '../app.module';

@Component({
  selector: 'ns-favoritos',
  templateUrl: './favoritos.component.html',
})
export class FavoritosComponent implements OnInit {

  resultados: Array<string>;

  constructor(private noticias: NoticiasService,
              private store: Store<AppState>,
              private router: Router,
              private routerExtensions: RouterExtensions) { }

  ngOnInit(): void {
    this.resultados= this.noticias.getFavoritos();
  }

  onButtonTap(): void {
    this.noticias.clearFavoritos();

  }

  onDrawerButtonTap(): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.showDrawer();
  }

  onLeerTap(x): void {
    console.log("Leer ahora pulsado!");
    this.store.dispatch(new LeerNoticiaAction(new Noticia(x)));
  }

}
