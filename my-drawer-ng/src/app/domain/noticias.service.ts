import { Injectable } from "@angular/core";
import { getJSON, request } from "tns-core-modules/http";
const sqlite = require("nativescript-sqlite");

@Injectable()
export class NoticiasService {

    api: string = "https://61ed5d5045a5.ngrok.io";
    private noticias: Array<string> = [];
    
    database: any;


    constructor(){
        var couchbaseModule = require("nativescript-couchbase"); 
        this.database = new couchbaseModule.Couchbase("test-database");
        
        this.getDb_logs((db) => {
            console.dir(db);
            console.log("LOGS");
            db.each("select * from logs",
                (err, fila) => console.log("fila: ", fila),
                (err, totales) => console.log("Filas totales: ", totales));
        }, () => console.log("error on getDB"));
        this.getDb_favs((db) => {
            console.dir(db);
            console.log("FAVS");
            db.each("select * from favs",
                (err, fila) => console.log("fila: ", fila),
                (err, totales) => console.log("Filas totales: ", totales));
        }, () => console.log("error on getDB"));

        this.database.createView("logs", "1",
                 (document, emitter) =>
                            emitter.emit(document._id, document));
         const rows = this.database.executeQuery("logs", {limit : 200});
         console.log("documentos: " + JSON.stringify(rows)); 
    }

    getFavoritos(){
        let resultados: Array<string>;
        resultados = [];
        
        this.getDb_favs((db) => {
            db.each("select * from favs",
                (err, fila) => {
                                console.log("fila fav: ", fila);
                                resultados.push(fila);
                               },
                (err, totales) => console.log("Filas favs totales: ", totales));

        }, () => console.log("error on getDB"));

        return resultados;
    }

    addFavoritos(s: string){    
        this.getDb_favs((db) => {
            db.each("select * from favs where texto = ?", [s],
                (err, fila) => console.log("fila fav: ", fila),
                (err, totales) => {
                                console.log("Filas favs totales: ", totales);
                                if (totales==0) {
                                    db.execSQL("insert into favs (texto) values (?)", [s],
                                         (err, id) => console.log("nuevo id: ", id));
                                } else {
                                    console.log("Este favorito ("+s+") ya esta añadido");
                                }
                            });

        }, () => console.log("error on getDB")); 
    }


    clearFavoritos(){
        this.getDb_favs((db) => {
            db.execSQL("delete from favs",
                (err, resultado) => console.log("Resultado clear fav: ", resultado));
        }, () => console.log("error on getDB"));
    }

    getDb_logs(fnOk, fnError){
        return new sqlite("mi_db_logs", (err, db) => {
            if (err) {
                console.error("Error al abrir db!", err);
            } else {
                console.log("Está la db abierta: ", db.isOpen() ? "Sí" : "No");
                db.execSQL("CREATE TABLE IF NOT EXISTS LOGS (id INTEGER PRIMARY KEY AUTOINCREMENT, texto TEXT)")
                    .then((id) => {
                        console.log("CREATE TABLE LOGS OK");
                        fnOk(db);
                    }, (error) => {
                        console.log("CREATE TABLE LOGS ERROR", error);
                        fnError(error);
                    });
            }
        });
    }

    getDb_favs(fnOk, fnError){
        return new sqlite("mi_db_logs", (err, db) => {
            if (err) {
                console.error("Error al abrir db!", err);
            } else {
                console.log("Está la db abierta: ", db.isOpen() ? "Sí" : "No");
                db.execSQL("CREATE TABLE IF NOT EXISTS FAVS (id INTEGER PRIMARY KEY AUTOINCREMENT, texto TEXT)")
                    .then((id) => {
                        console.log("CREATE TABLE FAVS OK");
                        fnOk(db);
                    }, (error) => {
                        console.log("CREATE TABLE FAVS ERROR", error);
                        fnError(error);
                    });
            }
        });
    }


    agregar(s: string){
        //this.noticias.push(s);
        return request({
            url: this.api + "/favs",
            method: "POST",
            headers: { "Content-Type": "application/json" },
            content: JSON.stringify({
                nuevo: s
            })
        });
    }

    buscar(s: string){
        //return this.noticias;
        this.getDb_logs((db) => {
            db.execSQL("insert into logs (texto) values (?)", [s],
                (err, id) => console.log("nuevo id: ", id));
        }, () => console.log("error on getDB"));

        const documentId = this.database.createDocument({ texto: s });
        console.log("nuevo id couchbase: ", documentId); 

        return getJSON(this.api + "/get?q=" + s);
    }


    sizeof(){
        return this.noticias.length;
    }

    favs() {
        return getJSON(this.api + "/favs");
    }
    
}