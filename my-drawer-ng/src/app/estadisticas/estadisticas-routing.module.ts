import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { MyStatsComponent } from "../my-stats/my-stats.component";
import { MyGraficComponent } from "../my-grafic/my-grafic.component";

const routes: Routes = [
    { path: "", component: MyStatsComponent },  
    { path: "grafic", component: MyGraficComponent },
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class EstadisticasRoutingModule { }