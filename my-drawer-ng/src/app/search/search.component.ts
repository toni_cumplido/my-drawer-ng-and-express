import { Component, ElementRef, OnInit, ViewChild  } from "@angular/core";
import { Router } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import * as Toast from 'nativescript-toasts'
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";


import { Color } from "tns-core-modules/color";
import { View } from "tns-core-modules/ui/core/view";
import { AnimationCurve } from 'tns-core-modules/ui/enums';
import { ActivityIndicator } from "tns-core-modules/ui/activity-indicator";

import { Noticia, NuevaNoticiaAction } from "../domain/noticias-state.model";
import { NoticiasService } from "../domain/noticias.service";
import { AppState } from "../app.module";
import { Store } from "@ngrx/store";


@Component({
    selector: "Search",
    templateUrl: "./search.component.html"/*,
    providers: [NoticiasService]*/
})
export class SearchComponent implements OnInit {

    resultados: Array<string>;
    @ViewChild("layout", { static: false }) layout: ElementRef;
    @ViewChild("icono", { static: false }) icono: ElementRef;

    constructor(private noticias: NoticiasService,
                private store: Store<AppState>,
                private router: Router,
                private routerExtensions: RouterExtensions) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
        /*
        this.noticias.agregar("hola 1!");
        this.noticias.agregar("hola 2!");
        this.noticias.agregar("hola 3!");
        */
       this.store.select((state) => state.noticias.sugerida)
            .subscribe((data) => {
                const f = data;
                if (f != null) {
                    Toast.show({text: "Sugerimos leer: "+ f.titulo, duration: Toast.DURATION.SHORT});
                }
            });
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onFavoritosTap(s: string): void{
        console.log("Añadiendo " + s + " a Favoritos");

        this.noticias.addFavoritos(s);

    }

    onItemTap(x): void {
        /*console.dir(x);
        this.routerExtensions.navigate(["/search/detalle"], {
            transition: {
                name: "fade"
            }
        });

        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.closeDrawer();*/

        this.store.dispatch(new NuevaNoticiaAction(new Noticia(x.view.bindingContext)));

    }

    doLater(fn) { setTimeout(fn, 1000); }

    onPull(e) {
        console.log(e);
        const pullRefresh = e.object;
        setTimeout(() => {
            this.noticias.agregar("hola "+ (this.noticias.sizeof()+1) + "!");
            pullRefresh.refreshing=false;
        },2000);
        const toastOptions: Toast.ToastOptions = {text: "Añadida noticia!", duration: Toast.DURATION.SHORT};
        this.doLater(() => Toast.show(toastOptions));

    }

    buscarAhora(s: string){
        //this.resultados = this.noticias.buscar().filter((x) => x.indexOf(s) >= 0);
        console.dir("buscarAhora "+ s);

        this.noticias.buscar(s).then((r:any) => {
            console.log("resultados buscarAhora: " + JSON.stringify(r));
            this.resultados = r;
        }, (e) => {
            console.log("error buscarAhora " + e);
            Toast.show({text: "Error en la búsqueda", duration: Toast.DURATION.SHORT});
        })

        const layout = <View>this.layout.nativeElement;
        layout.animate({
            backgroundColor: new Color("blue"),
            duration: 300,
            delay: 150
        }).then(() => layout.animate({
            backgroundColor: new Color("white"),
            duration: 300,
            delay: 150            
        }));       

        const icono=<View>this.icono.nativeElement;
        icono.animate({
            rotate: 360,
            curve: AnimationCurve.easeInOut,
            duration: 3000
        }).then(() => icono.animate ({
            // Reset animation
            rotate:0
        }));
    }

    cambio_busy (e) {
        let indicator = <ActivityIndicator>e.object;
        console.log("indicator.busy: " + indicator.busy);
    }
}
