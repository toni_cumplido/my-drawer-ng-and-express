import { Component, OnInit } from '@angular/core';
import * as app from "tns-core-modules/application";
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';
import { RouterExtensions } from 'nativescript-angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'ns-my-grafic',
  templateUrl: './my-grafic.component.html',
  styleUrls: ['./my-grafic.component.css']
})
export class MyGraficComponent implements OnInit {


  constructor(private router: Router, private routerExtensions: RouterExtensions) { }

  ngOnInit(): void {
  }

  onDrawerButtonTap(): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.showDrawer();
  }

  onGraficButtonTap(): void {
    this.routerExtensions.navigate(["/home"], {
      transition: {
          name: "fade"
      }
    });

    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.closeDrawer();
  }
}
