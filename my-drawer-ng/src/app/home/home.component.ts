import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { AppState } from "../app.module";
import { Store } from "@ngrx/store";

@Component({
    selector: "Home",
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {

    resultados: Array<string>;

    constructor(private store: Store<AppState>) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
        let i, array_hemeroteca;
        this.resultados = [];
        this.store.select((state) => state.noticias.hemeroteca)
                .subscribe((hemeroteca) => {
                    const f = hemeroteca;
                    hemeroteca.forEach(
                       (fila , i ) => { 
                            this.resultados.push(fila.titulo[1]); 
                                
                        });
                });
        
        console.dir(this.resultados);
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
