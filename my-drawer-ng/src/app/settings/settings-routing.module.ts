import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { SettingsComponent } from "./settings.component";
import { SettingsFormComponent } from "./settings-form.component";

const routes: Routes = [
    { path: "", component: SettingsComponent },
    { path: "EditaUsuario", component: SettingsFormComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class SettingsRoutingModule { }
