import { Component, OnInit  } from "@angular/core";
import * as appSettings from "tns-core-modules/application-settings"; 
import * as app from "tns-core-modules/application";
import { Router } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";

@Component({
    selector: "SettingsForm",
    templateUrl: "./settings-form.component.html"
    
})
export class SettingsFormComponent implements OnInit{
    usuario: string = "";
   
    constructor(private router: Router, private routerExtensions: RouterExtensions) { }
    
    ngOnInit(): void {      
        this.usuario = appSettings.getString("nombreUsuario", "Anónimo");
    }

    onButtonTap(): void {
        console.log(this.usuario);
        
        appSettings.setString("nombreUsuario", this.usuario);  
        
        this.routerExtensions.navigate(["/settings"], {
            transition: {
                name: "fade"
            }
          });
      
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.closeDrawer();
        
    }
 
}